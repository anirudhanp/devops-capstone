<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>All Test Details</title>
</head>
<body bgcolor="cyan" >
<h2>List of Candidates and Tests</h2>
<table border="3">
<tr>
	<th>Test Date</th>
	<th>Assessment</th>
	<th>Test Marks</th>
	<th>Email ID</th>
	<th>Total Marks</th>
	<th>Result</th>
</tr>
<c:forEach items="${reports}" var="report" varStatus="index">
	<tr>
		<td><span id = "testDate${index.count}">${report.getDate()}</span></td>
		<td><span id = "assessment${index.count}">${report.assessmentTopic}</span></td>
		<td><span id = "marks${index.count}">${report.getTestMarks()}</span></td>
		<td><span id = "email${index.count}">${report.getEmail()}</span></td>
		<td><span id = "totalMarks${index.count}">${report.getTotalMarks()}</span></td>
		<td><span id = "result${index.count}">${report.getResult()}</span></td>
	</tr>
</c:forEach>

</table>
</body>
</html>