package com.wipro.MyLearn;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyLearnDevOpsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyLearnDevOpsApplication.class, args);
	}

}
