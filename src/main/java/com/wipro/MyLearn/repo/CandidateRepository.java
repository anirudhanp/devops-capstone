package com.wipro.MyLearn.repo;

import org.springframework.stereotype.Repository;

import com.wipro.MyLearn.dao.CandidateDetails;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;


@Repository
public interface CandidateRepository extends JpaRepository<CandidateDetails, String> {
	
	public List<CandidateDetails> findAllByOrderByFirstNameAsc();

	public CandidateDetails findByEmail(String email);

	
}
