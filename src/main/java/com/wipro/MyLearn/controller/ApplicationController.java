package com.wipro.MyLearn.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.wipro.MyLearn.dao.Answers;
import com.wipro.MyLearn.dao.CandidateDetails;
import com.wipro.MyLearn.service.AssessmentService;





@Controller
public class ApplicationController {
	
	@Autowired
	AssessmentService assService;
	
	@GetMapping(value = "")
	public String homePage(Model m){
		m.addAttribute("Candidate", new CandidateDetails());
		return "HomePage";
	}
	
	@PostMapping(value="/login/do")
	public String userLogin(@ModelAttribute("Candidate")CandidateDetails cnd) {
		String userEmail = cnd.getEmail();
		String userPassword= cnd.getPassword();
		return assService.checkLoginData(userEmail, userPassword);
		
	}
	@GetMapping(value = "/register")
	public String registrationPage(Model m){
		m.addAttribute("newUser", new CandidateDetails());
		return "Registration";
	}
	@PostMapping(value = "/registration/do")
	public String addNewCandidate(@ModelAttribute("Candidate")CandidateDetails cnd){
		cnd.setRole("candidate");
		return assService.newRegister(cnd);
		
	}
	
	@GetMapping(value = "/springAssessment")
	public String getSpringAssessment(Model m) {
	m.addAttribute("springTest", new Answers());
	
	return "springAssessment";
		
	}
	@GetMapping(value = "/hibernateAssessment")
	public String getHibernateAssessment(Model m) {
	m.addAttribute("hibernateTest", new Answers());
	return "hibernateAssessment";
		
	}
	
	@PostMapping(value = "/evaluate/spring/test")
	public String springEvaluation(@ModelAttribute("springTest")Answers ans) {
		return assService.SpringAssessment(ans);
	}
	
	@PostMapping(value = "/evaluate/hibernate/test")
	public String hibernateEvaluation(@ModelAttribute("hibernateTest")Answers ans) {
		return assService.hibernateAssessment(ans);
	}
	
	@GetMapping(value = "/users/all")
	public String getAllUsers(ModelMap map) {
		map.addAttribute("usersList",assService.getUserDetails());
		return "AllUserList";
	}
	
	@GetMapping(value = "/tests/all")
	public String getAllTests(ModelMap map) {
		map.addAttribute("reports",assService.getTestList());
		return "TestDetails";
	}
	
}
