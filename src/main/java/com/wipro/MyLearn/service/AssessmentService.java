package com.wipro.MyLearn.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wipro.MyLearn.dao.Answers;
import com.wipro.MyLearn.dao.AssessmentDetails;
import com.wipro.MyLearn.dao.CandidateDetails;
import com.wipro.MyLearn.repo.AssessmentRepository;
import com.wipro.MyLearn.repo.CandidateRepository;




@Service
public class AssessmentService {
	
	@Autowired
	AssessmentRepository assessmentRepo;
	
	@Autowired
	CandidateRepository candidateRepo;
	
	
	SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");  
	
	

	
	private String mailId;

	public String getMailId() {
		return mailId;
	}

	public void setMailId(String mailId) {
		this.mailId = mailId;
	}
	public String checkLoginData(String email,String pwd) {
      
		try {
			setMailId(email);
		CandidateDetails cnd = candidateRepo.findByEmail(email);
		if(cnd==null) {
			return "InvalidUserPage";
		}
		else if(!pwd.equalsIgnoreCase(cnd.getPassword())){
			return "PasswordErrorPage"; 
		}
		else if("Candidate".equalsIgnoreCase(cnd.getRole())) {
			return "LoginSuccess";
		}
		else {
			return "AdminHomePage";
		}
		
	    }catch(Exception e){
 		return "InvalidUserPage";
	    }

	}
	
	public String newRegister(CandidateDetails candid) {
		
        candidateRepo.save(candid);		
		
		return "HomePage";
		
	}
	
	public String hibernateAssessment(Answers ans) {
		
		AssessmentDetails ass1 = new AssessmentDetails();
		Date date = new Date();	
		int count =0;
		
		if("Object Relational Mapping".equalsIgnoreCase(ans.getQues1())) {
			count++;
		}
		if("uni-directional & bi-directional".equalsIgnoreCase(ans.getQues2())) {
			count++;
		}
		if("configuration file".equalsIgnoreCase(ans.getQues3())) {
			count++;
		}
		if("Hibernate Query Language".equalsIgnoreCase(ans.getQues4())) {
			count++;
		}
		if("isolation levels".equalsIgnoreCase(ans.getQues5())) {
			count++;
		}
		
		
		ass1.setAssessmentTopic("Hibernate-L1");
		ass1.setEmail(getMailId());
		ass1.setTotalMarks(50);
		ass1.setTestMarks(count*10);
     	ass1.setDate(formatter.format(date));
     	
		if(count >= 3) {
			ass1.setResult("Passed");
			assessmentRepo.save(ass1);
			return "SuccessPage";
		}
		else {
			ass1.setResult("Failed");
			assessmentRepo.save(ass1);
			return "FailurePage";
		}
		
	}
	
public String SpringAssessment(Answers ans) {
		
	AssessmentDetails ass2 = new AssessmentDetails();
	Date date = new Date();	
	int count =0;
	
		if("J2EE App Development Framework".equalsIgnoreCase(ans.getQues1())) {
			count++;
		}
		if("Inversion Of Control".equalsIgnoreCase(ans.getQues2())) {
			count++;
		}
		if("Aspect Oriented Programming".equalsIgnoreCase(ans.getQues3())) {
			count++;
		}
		if("Application Context".equalsIgnoreCase(ans.getQues4())) {
			count++;
		}
		if("Dispatcher Servlet".equalsIgnoreCase(ans.getQues5())) {
			count++;
		}
		ass2.setEmail(getMailId());
		ass2.setAssessmentTopic("Spring-L1");
		ass2.setTotalMarks(50);
		ass2.setTestMarks(count*10);
     	ass2.setDate(formatter.format(date));
     	
		if(count >= 3) {
			ass2.setResult("Passed");
			assessmentRepo.save(ass2);
			return "SuccessPage";
		}
		else {
			ass2.setResult("Failed");
			assessmentRepo.save(ass2);
			return "FailurePage";
		}
		
	}

    public List<CandidateDetails> getUserDetails() {
	return candidateRepo.findAllByOrderByFirstNameAsc();
	}

	public List<AssessmentDetails> getTestList() {
		return assessmentRepo.findAll();
	}
	
}
