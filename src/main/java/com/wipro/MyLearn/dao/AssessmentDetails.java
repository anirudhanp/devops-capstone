package com.wipro.MyLearn.dao;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="AssessmentDetails")
public class AssessmentDetails {
	
	
	@Id
	@Column
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	

	@Column(name="Topic")
    private String assessmentTopic;
	
	
	@Column(name="Email")
    private String email;
	
	@Column(name="TotalMarks")
    private int totalMarks;
	
	@Column(name="TestMarks")
    private int testMarks;
	

	@Column(name="Result")
    private String result;
	
	
	@Column(name="Date")
    private String date;

	public AssessmentDetails(String assessmentTopic, String email, int totalMarks, int testMarks, String result,
			String date) {
		super();
		
		this.assessmentTopic = assessmentTopic;
		this.email = email;
		this.totalMarks = totalMarks;
		this.testMarks = testMarks;
		this.result = result;
		this.date = date;
	}

	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}
	
	public AssessmentDetails() {
		
	}


	public String getAssessmentTopic() {
		return assessmentTopic;
	}

	public void setAssessmentTopic(String assessmentTopic) {
		this.assessmentTopic = assessmentTopic;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getTotalMarks() {
		return totalMarks;
	}

	public void setTotalMarks(int totalMarks) {
		this.totalMarks = totalMarks;
	}

	public int getTestMarks() {
		return testMarks;
	}

	public void setTestMarks(int testMarks) {
		this.testMarks = testMarks;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}
}