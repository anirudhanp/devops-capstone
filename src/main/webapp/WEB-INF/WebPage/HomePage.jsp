<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Online Assessment - Home Page</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/Home.css" />
</head>
<body>

<section class='login' id='login'>
  <div class='head'>
  <img src="https://pxntxs.github.io/templates/login-page/i/telescope.png">
  <h1 class='company'>Wilearn Online Assessment</h1>
  </div>
  <p class='msg'>Please Login</p>
  <div class='form'>
  <form:form action="/MYLearnAssessment/login/do" modelAttribute="Candidate">
  <form:input type="email" placeholder='email' class='text' id="email" path="email" required="required"></form:input><br>
  <form:input type="password" placeholder='password' class='password' path="password" required="required"></form:input><br>
  <button id='do-login' class='btn-login' type="submit">LOGIN</button>
  <label for="sign_up">New User </label>
  <%String regPath = request.getContextPath();%>
  <a href="<%=regPath%>/register" class='signup' id="sign_up">Register Here</a>
   </form:form>
  </div>
</section>


</body>
</html>
