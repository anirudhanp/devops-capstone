package com.wipro.MyLearn.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.wipro.MyLearn.dao.AssessmentDetails;



@Repository
public interface AssessmentRepository extends JpaRepository<AssessmentDetails, Integer> {

	
}
