<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Select Reports</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/LoginSuccess.css" />
</head>
<header>
<div class="main-menu col-xs-12">
<%String basePath = request.getContextPath();%>
					<div class="main-bar">
						<a href="#" id="btn-menu"><i class="fa fa-bars" aria-hidden="true"></i> Menu</a>
						<a href="#" id="btn-search"><i class="fa fa-search" aria-hidden="true"></i></a>
					</div>
					<nav class="nav-bar">
						<ul>
							<li><a href="<%=basePath%>">WiLearn</a></li>
							<li><a href="<%=basePath%>/users/all">View list of all candidates</a></li>
	                        <li><a href="<%=basePath%>/tests/all">View list of all tests taken</a></li>
	                         <li style="margin-left:800px"><a href="<%=basePath%>/">Logout</i></a></li>
						</ul>
						<a href="#" id="btn-search2"><i class="fa fa-search" aria-hidden="true"></i></a>
					</nav>
				</div>
</header>
<body>
<h1 id="message"> Welcome Admin</h1>
</body>
</html>