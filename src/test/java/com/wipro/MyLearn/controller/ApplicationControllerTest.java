package com.wipro.MyLearn.controller;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.wipro.MyLearn.dao.CandidateDetails;
import com.wipro.MyLearn.repo.CandidateRepository;




@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
class ApplicationControllerTest extends Mockito{

	@Autowired
	private MockMvc mockMvc;
	
	@Autowired
	CandidateRepository candidateRepo;
	
	@InjectMocks private ApplicationController appController;
	
	
	@Before
    public void setUp() throws Exception {

        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(appController).build();
    }
	
	
	 @Test
	 public void testHomePage() throws Exception {
			this.mockMvc.perform(get("/"))
	        .andExpect(status().isOk())
	        .andExpect(view().name("HomePage"))
	        .andDo(MockMvcResultHandlers.print())
	        .andReturn();
		}
	 
	 @Test
	 public void testAdminLoginSuccess() throws Exception {
		
		 this.mockMvc.perform(post("/login/do")
					.param("email", "roger@gmail.com")
					.param("password", "wipro@123"))
		            .andExpect(status().isOk())
					.andExpect(view().name("AdminHomePage"));
		 
		}
	 
	 @Test
	 public void testUserRegistration() throws Exception {
		 this.mockMvc.perform(post("/registration/do")
				 .param("email", "ani@gmail.com")
				 .param("password", "wipro@123")
				 .param("firstName", "Ani")
				 .param("lastName", "P")
				 )
		            .andExpect(status().isOk())
					.andExpect(view().name("HomePage"));
		 
		}
	 
	@Test
	public void testCandidatesLoginSuccess() throws Exception {
		 CandidateDetails cnd = new CandidateDetails();
		 cnd.setEmail("ani@gmail.com");
		 cnd.setFirstName("Ani");
		 cnd.setLastName("P");
		 cnd.setPassword("wipro@123");
		 cnd.setRole("Candidate");
		candidateRepo.save(cnd);
	
	 this.mockMvc.perform(post("/login/do")
				.param("email", "ani@gmail.com")
				.param("password", "wipro@123"))
	            .andExpect(status().isOk())
				.andExpect(view().name("LoginSuccess"));

	}
	
	@Test
	public void testInvalidUserLogin() throws Exception {
	
	 this.mockMvc.perform(post("/login/do")
				.param("email", "rosger@gmail.com")
				.param("password", "wipro@123"))
	            .andExpect(status().isOk())
				.andExpect(view().name("InvalidUserPage"));

	}
	
	@Test
	public void testInvalidPasswordLogin() throws Exception {
	
	 this.mockMvc.perform(post("/login/do")
				.param("email", "roger@gmail.com")
				.param("password", "wiprdo@123"))
	            .andExpect(status().isOk())
				.andExpect(view().name("PasswordErrorPage"));

	}
	
	@Test
	public void testAllUsers() throws Exception {
	
	 this.mockMvc.perform(get("/users/all"))
	            .andExpect(status().isOk())
				.andExpect(view().name("AllUserList"));

	}
	
	@Test
	public void testAllTests() throws Exception {
	
	 this.mockMvc.perform(get("/tests/all"))
	            .andExpect(status().isOk())
				.andExpect(view().name("TestDetails"));

	}
}
