package com.wipro.MyLearn;

import static org.junit.jupiter.api.Assertions.*;

import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxBinary;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.junit.runner.RunWith;


@SpringBootTest
class LoginIT {

static WebDriver driver;
FirefoxBinary firefoxBinary = new FirefoxBinary();
FirefoxOptions firefoxOptions = new FirefoxOptions();

	
	@BeforeClass
	public void setUp() {
		
		WebDriverManager.firefoxdriver().setup();
		firefoxBinary.addCommandLineOptions("--headless");
		firefoxBinary.addCommandLineOptions("--disable-gpu");
        firefoxOptions.setBinary(firefoxBinary);
		    
	}
	
	@Test
	public void loginAsAdmin() throws Throwable {
		driver = new FirefoxDriver(firefoxOptions);
		driver.manage().window().maximize();
		driver.get("http://localhost:1718/MYLearnAssessment");
	    WebDriverWait wait = new WebDriverWait(driver, 15);
	    wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("company")));
		String actualString = driver.findElement(By.className("company")).getText();
		assertTrue(actualString.contains("Wilearn Online Assessment"));
		driver.findElement(By.id("email")).sendKeys("roger@gmail.com");
		driver.findElement(By.id("password")).sendKeys("wipro@123");
		driver.findElement(By.id("do-login")).click();
		String actualString2 = driver.findElement(By.id("message")).getText();
		assertTrue(actualString2.contains("Welcome Admin"));
		driver.quit();
	}
	
	@Test
	public void registrationTest() {
		driver = new FirefoxDriver(firefoxOptions);
		driver.manage().window().maximize();
		driver.get("http://localhost:1718/MYLearnAssessment");
		WebDriverWait wait = new WebDriverWait(driver, 15);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("company")));
        driver.findElement(By.id("sign_up")).click();
	    wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("header")));
	    driver.findElement(By.id("firstName")).sendKeys("Anirudhan");
	    driver.findElement(By.id("lastName")).sendKeys("Pillai");
	    driver.findElement(By.id("email")).sendKeys("ani@gmail.com");
	    driver.findElement(By.id("password")).sendKeys("wipro@123");
	    driver.findElement(By.className("button")).click();
	    wait.until(ExpectedConditions.visibilityOfElementLocated(By.className("company")));
	    driver.findElement(By.id("email")).clear();
	    driver.findElement(By.id("email")).sendKeys("ani@gmail.com");
	    driver.findElement(By.id("password")).clear();
		driver.findElement(By.id("password")).sendKeys("wipro@123");
		driver.findElement(By.id("do-login")).click();
		String actualString2 = driver.findElement(By.id("message")).getText();
		assertTrue(actualString2.contains("Welcome"));
		driver.quit();	    
	}

}
