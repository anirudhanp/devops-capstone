<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Registration Page</title>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/Registration.css" />
</head>
<body>


  <div id="header"> 
      <h1>Sign Up</h1> 
    </div>
    <div id="form">
     
      <form:form action="/MYLearnAssessment/registration/do" modelAttribute="newUser">
      
      <form:input type="text" placeholder="First Name" pattern="[A-Za-z]{1,}" path="firstName" required="required"></form:input>
      <form:input type="text" placeholder="Last Name" pattern="[A-Za-z]{1,}" path="lastName" required="required"></form:input>
      <form:input type="email" placeholder= "Email" path="email" required="required"></form:input>
      
      <form:input type="password" placeholder="Password" path="password" pattern=".{8,}" required="required"></form:input>
      <input class="button" type="submit" value="Sign Up">
     
      </form:form>
      <%String regPath = request.getContextPath();%>
      <p>Or need to <a href="<%=regPath%>">Login</a> ?</p>
    </div>

</body>
</html>