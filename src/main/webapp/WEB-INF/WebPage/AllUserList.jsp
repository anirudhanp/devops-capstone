<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>All User Details</title>
</head>
<body bgcolor="cyan">
<h2>List of All Users</h2>
<table border="3">
<tr>
	<th>First Name</th>
	<th>Last Name</th>
	<th>Email ID</th>
	<th>User Type</th>
	
</tr>
<c:forEach items="${usersList}" var="user"  varStatus="index">
<tr>
	<td><span id= "firstName${index.count}">${user.getFirstName()}</span></td>
	<td><span id="lastName${index.count}">${user.getLastName()}</span></td>
	<td><span id="email${index.count}">${user.getEmail()}</span></td>
	<td><span id="userType${index.count}">${user.getRole()}</span></td>
</tr>
</c:forEach>
</table>
</body>
</html>